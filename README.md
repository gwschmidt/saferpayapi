# README #

Java client for the Saferpay REST API 1.6 of the SIX Group

### What is this repository for? ###

* Complete Java client code to access the Saferpay REST API 1.6 of the SIX Group 
* 0.0.1

### How do I get set up? ###

This project compiles into a simple Jar
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Guido W Schmidt guido.w.schmidt@performancebuildings.ch