package ch.performancebuildings.saferpay.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.xml.bind.DatatypeConverter;

public class Util {

	private static final Logger log = Logger.getLogger(Util.class.getName());
	
	public static <T extends Enum<T>> EnumSet<T> getEnumSet(JsonArray arr, Class<T> clazz) {
		EnumSet<T> s = null;
		if (arr != null) {
			s = EnumSet.noneOf(clazz);
			Iterator<JsonValue> it = arr.iterator();
			while (it.hasNext()) {
				String str = it.next().toString();
				s.add(Enum.valueOf(clazz, str));
			}
		}
		return s;
	}

	public static <T extends Enum<T>> JsonArrayBuilder getJsonArray(EnumSet<T> s) {
		if (s != null) {
			JsonArrayBuilder build = Json.createArrayBuilder();
			Iterator<T> it = s.iterator();
			while (it.hasNext()) {
				build.add(it.next().name());
			}
			return build;
		}
		else {
			return null;
		}
	}
	
	/**
	 * Copy from https://saferpay.github.io/jsonapi/index.html
	 */
	public static JsonObject sendSaferPayRequest(URL sfpUrl, JsonObject request, String sfpLogin, String sfpPassword)
			throws IOException {
		// encode credentials
		String credential = sfpLogin + ":" + sfpPassword;
		String encodedCredentials = DatatypeConverter.printBase64Binary(credential.getBytes());

		// create connection
		HttpURLConnection connection = (HttpURLConnection) sfpUrl.openConnection();
		connection.setRequestProperty("connection", "close");
		connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", "Basic " + encodedCredentials);
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setUseCaches(false);

		// write JSON to output stream
		JsonWriter writer = Json.createWriter(connection.getOutputStream());
		writer.writeObject(request);
		writer.close();

		// send request
		int responseCode = connection.getResponseCode();

		if (log.isLoggable(Level.FINE)) {
			log.fine(String.format("Response code for call %s: %s", sfpUrl, responseCode));
		}

		// get correct input stream
		if (responseCode == 200) {
			return Json.createReader(connection.getInputStream()).readObject();
		} else if (connection.getHeaderField("content_type").equals("application/json")) {
			JsonObject jsonError = Json.createReader(connection.getErrorStream()).readObject();
			log.severe(String.format("Call failed: %s", sfpUrl));
			SaferPayError error = new SaferPayError(jsonError.toString(), responseCode);
			throw error;
		} else {
			String message = new String(connection.getErrorStream().readAllBytes());
			SaferPayError error = new SaferPayError(message, responseCode);
			throw error;
		}
	}
	
}
