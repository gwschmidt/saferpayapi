package ch.performancebuildings.saferpay.util;

import java.util.Arrays;
import java.util.List;

public class SaferPayConst {

	public static final String SAFERPAY_SPEC_VERSION = "1.6";
	public static final List<String> SAFERPAY_LANGUAGE_CODES = Arrays.asList("de", "en", "fr", "da", "cs", "es", "hr",
			"it", "hu", "nl", "no", "pl", "pt", "ru", "ro", "sk", "sl", "fi", "sv", "tr", "el", "ja", "zh");
	public static final String SAFERPAY_TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
}
