package ch.performancebuildings.saferpay.util;

import java.io.IOException;

import javax.json.JsonArray;
import javax.json.JsonObject;

import ch.performancebuildings.saferpay.model.ResponseHeader;

public class SaferPayError extends IOException {

	private static final long serialVersionUID = 1L;

	/** http response status. 0 means error has no HTTP context */
	private int httpStatus;

	/**
	 * Contains general informations about the response. mandatory
	 */
	private ResponseHeader responseHeader;
	/**
	 * What can be done to resolve the error? Possible values: ABORT, OTHER_MEANS,
	 * RETRY, RETRY_LATER. mandatory
	 */
	private String behavior;
	/**
	 * Name / id of the error. These names will not change, so you may parse these
	 * and attach your logic to the ErrorName. mandatory
	 */
	private String errorName;
	/**
	 * Description of the error. The contents of this element might change without
	 * notice, so do not parse it. mandatory
	 */
	private String errorMessage;
	/** Id of the failed transaction, if available */
	private String transactionId;
	/**
	 * More details, if available. Contents may change at any time, so don�t parse
	 * it.
	 */
	private String[] errorDetail;
	/** Name of acquirer (if declined by acquirer) or processor */
	private String processorName;
	/** Result code returned by acquirer or processor */
	private String processorResult;
	/** Message returned by acquirer or processor */
	private String processorMessage;
	
	private boolean internalError = false;

	public SaferPayError(String message, boolean isFieldName) {
		super(isFieldName ? "Field " + message + " is mandatory" : message);
	}
	
	public SaferPayError(String message, int httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public SaferPayError(JsonObject json) {
		super(json.getString("errorMessage"));
		try {
			transactionId = json.getString("TransactionId", null);
			JsonArray arr = json.getJsonArray("ErrorDetail");
			if (arr != null) {
				errorDetail = new String[arr.size()];
				for (int i = 0; i < arr.size(); i++) {
					errorDetail[i] = arr.getString(i);
				}
			}
			processorResult = json.getString("ProcessorResult", null);
			processorMessage = json.getString("ProcessorMessage", null);
			behavior = json.getString("Behavior");
			errorName = json.getString("ErrorName");
			errorMessage = json.getString("errorMessage");
			processorName = json.getString("ProcessorName");
			responseHeader = new ResponseHeader("Error.ResponseHeader", json.getJsonObject("ResponseHeader"));
		}
		catch (SaferPayError e) {
			internalError = true;
		}
	}

	public int getHttpStatus() {
		return httpStatus;
	}

	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public String getBehavior() {
		return behavior;
	}
	
	public String getErrorName() {
		return errorName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String[] getErrorDetail() {
		return errorDetail;
	}

	public String getProcessorName() {
		return processorName;
	}

	public String getProcessorResult() {
		return processorResult;
	}

	public String getProcessorMessage() {
		return processorMessage;
	}

	public boolean isInternalError() {
		return internalError;
	}

}
