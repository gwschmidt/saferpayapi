package ch.performancebuildings.saferpay.util;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SaferPayConfig {

	private boolean testServer;

	private String login;
	private String password;

	private URL returnServletURL;
	private String ipAddress;

	private String requestHeaderCustomerId;
	private String terminalId;

	private List<String> paymentMethods;
	private List<String> wallets;

	private String orderIdSuffix;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRequestHeaderCustomerId() {
		return requestHeaderCustomerId;
	}

	public void setRequestHeaderCustomerId(String requestHeaderCustomerId) {
		this.requestHeaderCustomerId = requestHeaderCustomerId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public URL getReturnServletURL() {
		return returnServletURL;
	}

	public void setReturnServletURL(URL returnServletURL) {
		this.returnServletURL = returnServletURL;
	}

	public List<String> getPaymentMethods() {
		if (paymentMethods == null) {
			paymentMethods = new ArrayList<String>();
		}
		return paymentMethods;
	}

	public List<String> getWallets() {
		if (wallets == null) {
			wallets = new ArrayList<String>();
		}
		return wallets;
	}

	public void setPaymentMethods(List<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public void setWallets(List<String> wallets) {
		this.wallets = wallets;
	}

	public boolean isTestServer() {
		return testServer;
	}

	public void setTestServer(boolean testServer) {
		this.testServer = testServer;
	}

	public String getOrderIdSuffix() {
		return orderIdSuffix;
	}

	public void setOrderIdSuffix(String orderIdSuffix) {
		this.orderIdSuffix = orderIdSuffix;
	}

	public String orderIdSuffix() {
		return orderIdSuffix == null ? "" : orderIdSuffix;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
