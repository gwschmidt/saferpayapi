package ch.performancebuildings.saferpay;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.JsonObject;

import ch.performancebuildings.saferpay.model.AssertRequest;
import ch.performancebuildings.saferpay.model.AssertResponse;
import ch.performancebuildings.saferpay.model.InitializeRequest;
import ch.performancebuildings.saferpay.model.InitializeResponse;
import ch.performancebuildings.saferpay.util.SaferPayConfig;
import ch.performancebuildings.saferpay.util.Util;

public class PaymentPageApi {

	private static final String SAFERPAY_TEST_INITIALIZE_URL = "https://test.saferpay.com/api/Payment/v1/PaymentPage/Initialize";
	private static final String SAFERPAY_TEST_ASSERT_URL = "https://test.saferpay.com/api/Payment/v1/PaymentPage/Assert";
	private static final String SAFERPAY_INITIALIZE_URL = "https://www.saferpay.com/api/Payment/v1/PaymentPage/Initialize";
	private static final String SAFERPAY_ASSERT_URL = "https://www.saferpay.com/api/Payment/v1/PaymentPage/Assert";

	private static final Logger log = Logger.getLogger(PaymentPageApi.class.getName());

	private SaferPayConfig config;

	public PaymentPageApi(SaferPayConfig config) {
		this.config = config;
	}

	public InitializeResponse initialize(InitializeRequest request) {
		InitializeResponse result = null;
		try {
			URL saferPayURL = new URL(config.isTestServer() ? SAFERPAY_TEST_INITIALIZE_URL : SAFERPAY_INITIALIZE_URL);
			JsonObject req = request.toJsonBuilder().build();
			if (log.isLoggable(Level.FINE)) {
				log.fine("PaymentPage/Initialize in: " + req.toString());
			}
			JsonObject resp = Util.sendSaferPayRequest(saferPayURL, req, config.getLogin(), config.getPassword());
			if (log.isLoggable(Level.FINE)) {
				log.fine("PaymentPage/Initialize out: " + resp.toString());
			}
			result = new InitializeResponse("initialize.Response", resp);
		} catch (IOException e) {
			log.log(Level.SEVERE, "initialize", e);
		}
		return result;
	}

	public AssertResponse _assert(AssertRequest request) {
		AssertResponse result = null;
		try {
			URL saferPayURL = new URL(config.isTestServer() ? SAFERPAY_TEST_ASSERT_URL : SAFERPAY_ASSERT_URL);
			JsonObject req = request.toJsonBuilder().build();
			if (log.isLoggable(Level.FINE)) {
				log.fine("PaymentPage/Assert in: " + req.toString());
			}
			JsonObject resp = Util.sendSaferPayRequest(saferPayURL, req, config.getLogin(), config.getPassword());
			if (log.isLoggable(Level.FINE)) {
				log.fine("PaymentPage/Assert out: " + resp.toString());
			}
			result = new AssertResponse("assert.Response", resp);
		} catch (IOException e) {
			log.log(Level.SEVERE, "_assert", e);
		}
		return result;
	}

}
