package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class BankAccount {

	/**
	 * mandatory, string	
	 * International Bank Account Number in electronical format (without spaces).
     * AlphaNumeric[1..50]
     * Example: DE12500105170648489890
	 */
	private String iBAN;
	/**
	 * string Name of the account holder Iso885915[1..50] Example: John Doe
	 */
	private String holderName;
	/**
	 * string Bank Identifier Code without spaces. AlphaNumeric[8..11] Example:
	 * INGDDEFFXXX
	 */
	private String bIC;
	/**
	 * string Name of the Bank
	 */
	private String bankName;
	/**
	 * string	
	 * ISO 2-letter country code of the Iban origin (if available) Example: CH
	 */
	private String countryCode;

	public static BankAccount get(JsonObject object) {
		if (object != null) {
			BankAccount obj = new BankAccount();
			obj.iBAN = object.getString("IBAN");
			obj.holderName = object.getString("HolderName", null);
			obj.bIC = object.getString("BIC", null);
			obj.bankName = object.getString("BankName", null);
			obj.countryCode = object.getString("CountryCode", null);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("IBAN", iBAN);
		if (holderName != null)
			build.add("HolderName", holderName);
		if (bIC != null)
			build.add("BIC", bIC);
		if (bankName != null)
			build.add("BankName", bankName);
		if (countryCode != null)
			build.add("CountryCode", countryCode);
		return build;
	}

	public String getiBAN() {
		return iBAN;
	}

	public void setiBAN(String iBAN) {
		this.iBAN = iBAN;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getbIC() {
		return bIC;
	}

	public void setbIC(String bIC) {
		this.bIC = bIC;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
