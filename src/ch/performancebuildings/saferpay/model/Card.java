package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Card {

	/** mandatory, string	
	 * Masked card number
	 * Example: 912345xxxxxx1234 */
	private String maskedNumber;
	/** mandatory, integer	
	 * Year of expiration
	 * Example: 2015 */
	private int expYear;
	/** mandatory, integer	
	 * Month of expiration (eg 9 for September)
	 * Example: 9 */
	private int expMonth;
	/** string	
	 * Name of the card holder (if known)
	 * Example: John Doe */
	private String holderName;
	/** string	
	 * ISO 2-letter country code of the card origin (if available)
	 * Example: CH */
	private String countryCode;
	/** string	
	 * The HashValue, if the hash generation is configured for the customer. */
	private String hashValue;
	
	public static Card get(JsonObject object) {
		if (object != null) {
			Card card = new Card();
			card.maskedNumber = object.getString("MaskedNumber");
			card.expYear = object.getInt("ExpYear");
			card.expMonth = object.getInt("ExpMonth");
			card.holderName = object.getString("HolderName", null);
			card.countryCode = object.getString("CountryCode", null);
			card.hashValue = object.getString("HashValue", null);
			return card;
		}
		else {
			return null;
		}
	}
	
	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("MaskedNumber", maskedNumber);
		build.add("ExpYear", expYear);
		build.add("ExpMonth", expMonth);
		if (holderName != null) build.add("HolderName", holderName);
		if (countryCode != null) build.add("CountryCode", countryCode);
		if (hashValue != null) build.add("HashValue", hashValue);
		return build;
	}

	public String getMaskedNumber() {
		return maskedNumber;
	}

	public void setMaskedNumber(String maskedNumber) {
		this.maskedNumber = maskedNumber;
	}

	public int getExpYear() {
		return expYear;
	}

	public void setExpYear(int expYear) {
		this.expYear = expYear;
	}

	public int getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(int expMonth) {
		this.expMonth = expMonth;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}
	
}

