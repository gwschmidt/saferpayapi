package ch.performancebuildings.saferpay.model;

public enum PaymentMethod {

	AMEX, 
	DIRECTDEBIT, 
	INVOICE, 
	BONUS, 
	DINERS, 
	EPRZELEWY, 
	EPS, 
	GIROPAY, 
	IDEAL, 
	JCB, 
	MAESTRO,
	MASTERCARD, 
	MYONE, 
	PAYPAL, 
	POSTCARD, 
	POSTFINANCE, 
	SAFERPAYTEST, 
	SOFORT, 
	VISA, 
	VPAY
}
