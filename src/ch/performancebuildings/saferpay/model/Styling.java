package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Styling {

	/** string	
	 * Custom styling resource (url) which will be referenced in web pages displayed by Saferpay to apply your custom styling.
	 * This file must be hosted on a SSL/TLS secured web server (the url must start with https://).
	 * Example: https://merchanthost/merchant.css */
	private String cssUrl;
	/** string	
	 * This parameter let you customize the appearance of the displayed payment pages. Per default a lightweight responsive styling will be applied.
	 * If you don't want any styling use 'NONE'.
	 * Possible values: DEFAULT, SIX, NONE.
	 * Example: DEFAULT*/
	private Theme theme;
	
	public static Styling get(JsonObject object) {
		if (object != null) {
			Styling obj = new Styling();
			obj.cssUrl = object.getString("CssUrl", null);
			String var = object.getString("Theme", null);
			if (var != null) obj.theme = Theme.valueOf(var);
			return obj;
		}
		else {
			return null;
		}
	}
	
	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (cssUrl != null) build.add("CssUrl", cssUrl);
		if (theme != null) build.add("Theme", theme.name());
		return build;
	}
	
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return cssUrl == null && theme == null;
	}

	public String getCssUrl() {
		return cssUrl;
	}

	public void setCssUrl(String cssUrl) {
		this.cssUrl = cssUrl;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	
}
