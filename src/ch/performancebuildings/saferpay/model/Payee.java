package ch.performancebuildings.saferpay.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Payee {

	/**
	 * mandatory, string International Bank Account Number in electronical format
	 * (without spaces). AlphaNumeric[1..50] Example: DE12500105170648489890
	 */
	private String iBAN;
	/**
	 * string Name of the account holder Iso885915[1..50] Example: John Doe
	 */
	private String holderName;
	/**
	 * string Bank Identifier Code without spaces. AlphaNumeric[8..11] Example:
	 * INGDDEFFXXX
	 */
	private String bIC;
	/**
	 * string Name of the Bank
	 */
	private String bankName;
	/**
	 * string The reason for transfer to be stated when paying the invoice (transfer
	 * of funds)
	 */
	private String reasonForTransfer;
	/**
	 * date The date by which the invoice needs to be settled
	 */
	private LocalDate dueDate;

	public static Payee get(JsonObject object) {
		if (object != null) {
			String var;
			Payee obj = new Payee();
			obj.iBAN = object.getString("IBAN");
			obj.holderName = object.getString("HolderName", null);
			obj.bIC = object.getString("BIC", null);
			obj.bankName = object.getString("BankName", null);
			obj.reasonForTransfer = object.getString("ReasonForTransfer", null);
			var = object.getString("DueDate", null);
			if (var != null)
				obj.dueDate = LocalDate.parse(var, DateTimeFormatter.ISO_DATE);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("IBAN", iBAN);
		if (holderName != null)
			build.add("HolderName", holderName);
		if (bIC != null)
			build.add("BIC", bIC);
		if (bankName != null)
			build.add("BankName", bankName);
		if (reasonForTransfer != null)
			build.add("ReasonForTransfer", reasonForTransfer);
		if (dueDate != null)
			build.add("DueDate", dueDate.format(DateTimeFormatter.ISO_DATE));
		return build;
	}

	public String getiBAN() {
		return iBAN;
	}

	public void setiBAN(String iBAN) {
		this.iBAN = iBAN;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getbIC() {
		return bIC;
	}

	public void setbIC(String bIC) {
		this.bIC = bIC;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getReasonForTransfer() {
		return reasonForTransfer;
	}

	public void setReasonForTransfer(String reasonForTransfer) {
		this.reasonForTransfer = reasonForTransfer;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

}
