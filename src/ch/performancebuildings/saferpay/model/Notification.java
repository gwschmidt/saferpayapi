package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Notification {

	/** string	
	 * Email address to which a confirmation email will be sent for successful authorizations. This overrides the email address configured in the back office (if any).
	 * Example: merchant@saferpay.com */
	private String merchantEmail;
	/** string	
	 * Email address to which a confirmation email will be sent for successful authorizations.
	 * Example: merchant@saferpay.com */
	private String payerEmail;
	/** string	
	 * Url to which Saferpay will post the asynchronous confirmation for the transaction. Supported schemes are http and https. You also have to make sure to support the POST-method.
	 */
	private String notifyUrl;
	
	public static Notification get(JsonObject object) {
		if (object != null) {
			Notification obj = new Notification();
			obj.merchantEmail = object.getString("MerchantEmail", null);
			obj.payerEmail = object.getString("PayerEmail", null);
			obj.notifyUrl = object.getString("NotifyUrl", null);
			return obj;
		}
		else {
			return null;
		}
	}

	public Notification() {
		super();
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (merchantEmail != null) build.add("MerchantEmail", merchantEmail);
		if (payerEmail != null) build.add("PayerEmail", payerEmail);
		if (notifyUrl != null) build.add("NotifyUrl", notifyUrl);
		return build;
	}
	
	public boolean isEmpty() {
		return merchantEmail == null && payerEmail == null && notifyUrl == null;
	}
	
	public String getMerchantEmail() {
		return merchantEmail;
	}
	public void setMerchantEmail(String merchantEmail) {
		this.merchantEmail = merchantEmail;
	}
	public String getPayerEmail() {
		return payerEmail;
	}
	public void setPayerEmail(String payerEmail) {
		this.payerEmail = payerEmail;
	}
	public String getNotifyUrl() {
		return notifyUrl;
	}
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
	
	
}
