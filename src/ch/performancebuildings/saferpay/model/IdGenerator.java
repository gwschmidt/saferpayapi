package ch.performancebuildings.saferpay.model;

public enum IdGenerator {

	MANUAL, RANDOM, RANDOM_UNIQUE
}
