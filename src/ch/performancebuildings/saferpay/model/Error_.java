package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Error_ {

	/**
	 * string Name / id of the error. Example: ErrorName
	 */
	private String errorName;
	/**
	 * string Description of the error. Example: ErrorMessage
	 */
	private String errorMessage;

	public static Error_ get(JsonObject object) {
		if (object != null) {
			Error_ obj = new Error_();
			obj.errorName = object.getString("ErrorName");
			obj.errorMessage = object.getString("ErrorMessage");
			return null;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("ErrorName", errorName);
		build.add("ErrorMessage", errorMessage);
		return build;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
