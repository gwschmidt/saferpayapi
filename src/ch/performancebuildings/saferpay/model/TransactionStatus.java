package ch.performancebuildings.saferpay.model;

public enum TransactionStatus {

	AUTHORIZED, CAPTURED
}
