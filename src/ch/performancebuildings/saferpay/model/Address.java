package ch.performancebuildings.saferpay.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Address {

	/**
	 * string The payers first name Max length: 100 Example: John
	 */
	private String firstName;
	/**
	 * string The payers last name Max length: 100 Example: Doe
	 */
	private String lastName;
	/**
	 * string The payers date of birth in ISO 8601 extended date notation YYYY-MM-DD
	 * AlphaNumeric[11..11] Example: 1990-05-31
	 */
	private LocalDate dateOfBirth;
	/**
	 * string The payers company Max length: 100 Example: ACME Corp.
	 */
	private String company;
	/**
	 * string The payers gender Possible values: MALE, FEMALE, COMPANY. Example:
	 * COMPANY
	 */
	private Gender gender;
	/**
	 * string The payers legal form Possible values: AG, GmbH, Misc. Example: AG
	 */
	private LegalForm legalForm;
	/**
	 * string The payers street Max length: 100 Example: Bakerstreet 32
	 */
	private String street;
	/**
	 * string The payers street, second line. Only use this, if you need two lines.
	 * It may not be supported by all acquirers. Max length: 100 Example: Stewart
	 * House
	 */
	private String street2;
	/**
	 * string The payers zip code Max length: 100 Example: 8000
	 */
	private String zip;
	/**
	 * string The payers city Max length: 100 Example: Zurich
	 */
	private String city;
	/**
	 * string The payers country subdivision code ISO 3166-2 country subdivision
	 * code Max length: 3 Min length: 1 Example: ZH
	 */
	private String countrySubdivisionCode;
	/**
	 * string The payers country subdivision code ISO 3166-1 alpha-2 country code
	 * Min length: 2 Max length: 2 Example: CH
	 */
	private String countryCode;
	/**
	 * string The payers phone number Max length: 100 Example: +41 12 345 6789
	 */
	private String phone;
	/**
	 * string The payers email Example: payer@provider.com
	 */
	private String email;

	public static Address get(JsonObject jsonObject) {
		if (jsonObject != null) {
			String val;
			Address obj = new Address();
			obj.firstName = jsonObject.getString("FirstName", null);
			obj.lastName = jsonObject.getString("LastName", null);
			val = jsonObject.getString("DateOfBirth", null);
			if (val != null)
				obj.dateOfBirth = LocalDate.parse(val, DateTimeFormatter.ISO_LOCAL_DATE);
			obj.company = jsonObject.getString("Company", null);
			val = jsonObject.getString("Gender", null);
			if (val != null)
				obj.gender = Gender.valueOf(val);
			val = jsonObject.getString("LegalForm", null);
			if (val != null)
				obj.legalForm = LegalForm.valueOf(val);
			obj.street = jsonObject.getString("Street", null);
			obj.street2 = jsonObject.getString("Street2", null);
			obj.zip = jsonObject.getString("Zip", null);
			obj.city = jsonObject.getString("City", null);
			obj.countrySubdivisionCode = jsonObject.getString("CountrySubdivisionCode", null);
			obj.countryCode = jsonObject.getString("CountryCode", null);
			obj.phone = jsonObject.getString("Phone", null);
			obj.email = jsonObject.getString("Email", null);
			return obj;
		}
		return null;
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (firstName != null)
			build.add("FirstName", firstName);
		if (lastName != null)
			build.add("LastName", lastName);
		if (dateOfBirth != null)
			build.add("DateOfBirth", dateOfBirth.format(DateTimeFormatter.ISO_LOCAL_DATE));
		if (company != null)
			build.add("Company", company);
		if (gender != null)
			build.add("Gender", gender.name());
		if (legalForm != null)
			build.add("LegalForm", legalForm.name());
		if (street != null)
			build.add("Street", street);
		if (street2 != null)
			build.add("Street2", street2);
		if (zip != null)
			build.add("Zip", zip);
		if (city != null)
			build.add("City", city);
		if (countrySubdivisionCode != null)
			build.add("CountrySubdivisionCode", countrySubdivisionCode);
		if (countryCode != null)
			build.add("CountryCode", countryCode);
		if (phone != null)
			build.add("Phone", phone);
		if (email != null)
			build.add("Email", email);
		return build;
	}

	public boolean isEmpty() {
		return firstName == null && lastName == null && dateOfBirth == null && company == null && gender == null
				&& legalForm == null && street == null && street2 == null && zip == null && city == null
				&& countrySubdivisionCode == null && countryCode == null && phone == null && email == null;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LegalForm getLegalForm() {
		return legalForm;
	}

	public void setLegalForm(LegalForm legalForm) {
		this.legalForm = legalForm;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountrySubdivisionCode() {
		return countrySubdivisionCode;
	}

	public void setCountrySubdivisionCode(String countrySubdivisionCode) {
		this.countrySubdivisionCode = countrySubdivisionCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
