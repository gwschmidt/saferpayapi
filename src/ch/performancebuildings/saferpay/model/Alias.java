package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Alias {

	/**
	 * mandatory, string Id / name of alias Example: alias35nfd9mkzfw0x57iwx
	 */
	private String id;
	/**
	 * mandatory, integer Number of days this card is stored within Saferpay.
	 * Minimum 1 day, maximum 1600 days. Example: 1000
	 */
	private int lifetime;

	public static Alias get(JsonObject object) {
		if (object != null) {
			Alias obj = new Alias();
			obj.id = object.getString("id");
			obj.lifetime = object.getInt("lifeTime");
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Id", id);
		build.add("Lifetime", lifetime);
		return build;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLifetime() {
		return lifetime;
	}

	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

}
