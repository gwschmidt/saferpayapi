package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class ReturnUrls {

	/**
	 * mandatory, string Return url for successful transaction Example:
	 * https://merchanthost/success
	 */
	private String success;
	/**
	 * mandatory, string Return url for failed transaction Example:
	 * https://merchanthost/fail
	 */
	private String fail;
	/**
	 * string Return url for transaction aborted by the payer. Example:
	 * https://merchanthost/abort
	 */
	private String abort;

	public ReturnUrls() {
		super();
	}

	public ReturnUrls(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		success = object.getString("Success");
		fail = object.getString("Fail");
		abort = object.getString("Abort", null);
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Success", success);
		build.add("Fail", fail);
		if (abort != null)
			build.add("Abort", abort);
		return build;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getFail() {
		return fail;
	}

	public void setFail(String fail) {
		this.fail = fail;
	}

	public String getAbort() {
		return abort;
	}

	public void setAbort(String abort) {
		this.abort = abort;
	}

}
