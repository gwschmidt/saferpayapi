package ch.performancebuildings.saferpay.model;

import java.util.EnumSet;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.Util;

public class AddressForm {

	/** mandatory, boolean	
	 * Specifes whether to show address form or not. */
	private boolean display;
	/** string[]	
	 * List of fields which the payer must enter to proceed with the payment.
	 * Possible values: CITY, COMPANY, COUNTRY, EMAIL, FIRSTNAME, LASTNAME, PHONE, SALUTATION, STATE, STREET, ZIP.
	 * Example: ["FIRSTNAME", "LASTNAME", "PHONE"]*/
	private EnumSet<AddressFormField> mandatoryFields;
	
	public static AddressForm get(JsonObject object) {
		if (object != null) {
			AddressForm obj = new AddressForm();
			obj.display = object.getBoolean("Display");
			obj.mandatoryFields= Util.getEnumSet(object.getJsonArray("MandatoryFields"), AddressFormField.class);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Display", display);
		if (mandatoryFields != null) build.add("MandatoryFields", Util.getJsonArray(mandatoryFields));
		return null;
	}

	public boolean isDisplay() {
		return display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public EnumSet<AddressFormField> getMandatoryFields() {
		return mandatoryFields;
	}

	public void setMandatoryFields(EnumSet<AddressFormField> mandatoryFields) {
		this.mandatoryFields = mandatoryFields;
	}

}
