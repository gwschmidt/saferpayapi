package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class DirectDebit {

	/**
	 * mandatory, string The unique Mandate reference, required for german direct
	 * debit payments.
	 */
	private String mandateId;
	/**
	 * mandatory, string Creditor id, required for german direct debit payments.
	 */
	private String creditorId;

	public static DirectDebit get(JsonObject object) {
		if (object != null) {
			DirectDebit obj = new DirectDebit();
			obj.mandateId = object.getString("MandateId");
			obj.creditorId = object.getString("CreditorId");
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("MandateId", mandateId);
		build.add("CreditorId", creditorId);
		return build;
	}

	public String getMandateId() {
		return mandateId;
	}

	public void setMandateId(String mandateId) {
		this.mandateId = mandateId;
	}

	public String getCreditorId() {
		return creditorId;
	}

	public void setCreditorId(String creditorId) {
		this.creditorId = creditorId;
	}

}
