package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Installment {

	/**
	 * mandatory, boolean If set to true, the authorization may later be referenced
	 * for recurring followup transactions.
	 */
	private boolean initial;

	public static Installment get(JsonObject object) {
		if (object != null) {
			Installment obj = new Installment();
			obj.initial = object.getBoolean("Initial");
			return obj;
		}
		else {
			return null;
		}
	}

	public Installment() {
		super();
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Initial", initial);
		return build;
	}

	public boolean isInitial() {
		return initial;
	}

	public void setInitial(boolean initial) {
		this.initial = initial;
	}

}
