package ch.performancebuildings.saferpay.model;

public enum LegalForm {

	AG, GmbH, Misc
}
