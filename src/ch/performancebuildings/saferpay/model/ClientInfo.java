package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class ClientInfo {

	/**
	 * string Name and version of the shop software Iso885915[1..100] Example: My
	 * Shop
	 */
	private String shopInfo;
	/**
	 * string Information on the operating system Iso885915[1..100] Example: Windows
	 * Server 2013
	 */
	private String osInfo;

	public static ClientInfo get(JsonObject object) {
		if (object != null) {
			ClientInfo obj = new ClientInfo();
			obj.shopInfo = object.getString("shopInfo", null);
			obj.osInfo = object.getString("osInfo", null);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (shopInfo != null)
			build.add("shopInfo", shopInfo);
		if (osInfo != null)
			build.add("osInfo", osInfo);
		return build;
	}

	public boolean isEmpty() {
		return shopInfo == null && osInfo == null;
	}

	public String getShopInfo() {
		return shopInfo;
	}

	public void setShopInfo(String shopInfo) {
		this.shopInfo = shopInfo;
	}

	public String getOsInfo() {
		return osInfo;
	}

	public void setOsInfo(String osInfo) {
		this.osInfo = osInfo;
	}

}
