package ch.performancebuildings.saferpay.model;

public enum TransactionType {

	PAYMENT, REFUND
}
