package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class RegistrationResult {

	/**
	 * mandatory, boolean Result of registration
	 */
	private boolean success;
	/**
	 * container If Success is 'true', information about the alias
	 */
	private Alias alias;
	/**
	 * container If Success is 'false', information on why the registration failed
	 */
	private Error_ error;

	public static RegistrationResult get(JsonObject object) {
		if (object != null) {
			RegistrationResult obj = new RegistrationResult();
			obj.success = object.getBoolean("Success");
			obj.alias = Alias.get(object.getJsonObject("Alias"));
			obj.error = Error_.get(object.getJsonObject("Error"));
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Success", success);
		if (alias != null)
			build.add("Alias", alias.toJsonBuilder());
		if (error != null)
			build.add("Error", error.toJsonBuilder());
		return build;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Error_ getError() {
		return error;
	}

	public void setError(Error_ error) {
		this.error = error;
	}

}
