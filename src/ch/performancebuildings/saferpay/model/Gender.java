package ch.performancebuildings.saferpay.model;

public enum Gender {

	MALE, FEMALE, COMPANY
}
