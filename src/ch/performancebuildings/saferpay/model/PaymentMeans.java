package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class PaymentMeans {

	/** mandatory, container	
	 * Brand information */
	private Brand brand;
	/** mandatory, string	
	 * Means of payment formatted / masked for display purposes
	 * Example: DisplayText */
	private String displayText;
	/** string	
	 * Name of Wallet, if the transaction was done by a wallet
	 * Example: MasterPass */
	private String wallet;
	/** container	
	 * Card data */
	private Card card;
	/** container	
	 * Bank account data for direct debit transactions. */
	private BankAccount bankAccount;
	
	public PaymentMeans() {
		super();
	}
	
	public PaymentMeans(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		brand = new Brand("PaymentMeans.Brand", object.getJsonObject("Brand"));
		displayText = object.getString("DisplayText");
		wallet = object.getString("Wallet");
		card = Card.get(object.getJsonObject("Card"));
		bankAccount = BankAccount.get(object.getJsonObject("BankAccount"));
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Brand", brand.toJsonBuilder());
		build.add("DisplayText", getDisplayText());
		build.add("Wallet", getWallet());
		if (card != null) build.add("Card", card.toJsonBuilder());
		if (bankAccount != null ) build.add("BankAccount", bankAccount.toJsonBuilder());
		return build;
	}

	public String getDisplayText() {
		return displayText;
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	public String getWallet() {
		return wallet;
	}

	public void setWallet(String wallet) {
		this.wallet = wallet;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}
	
}
