package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Payer {

	/**
	 * string IPv4 address of the card holder / payer if available. Dotted quad
	 * notation. Example: 111.111.111.111
	 */
	private String ipAddress;
	/**
	 * string Language to be used if Saferpay displays something to the payer.
	 *
	 * Code-List: de - German en - English fr - French da - Danish cs - Czech es -
	 * Spanish hr - Croatian it - Italian hu - Hungarian nl - Dutch no - Norwegian
	 * pl - Polish pt - Portuguese ru - Russian ro - Romanian sk - Slovak sl -
	 * Slovenian fi - Finnish sv - Swedish tr - Turkish el - Greek ja - Japanese zh
	 * - Chinese Example: de
	 */
	private String languageCode;
	/**
	 * container Information on the payers delivery address
	 */
	private Address deliveryAddress;
	/**
	 * container Information on the payers billing address
	 */
	private Address billingAddress;

	public static Payer get(JsonObject object) {
		if (object != null) {
			Payer obj = new Payer();
			obj.ipAddress = object.getString("IpAddress", null);
			obj.languageCode = object.getString("LanguageCode", null);
			obj.deliveryAddress = Address.get(object.getJsonObject("DeliveryAddress"));
			obj.billingAddress = Address.get(object.getJsonObject("BillingAddress"));
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (ipAddress != null)
			build.add("IpAddress", ipAddress);
		if (languageCode != null)
			build.add("LanguageCode", languageCode);
		if (deliveryAddress != null && !deliveryAddress.isEmpty())
			build.add("DeliveryAddress", deliveryAddress.toJsonBuilder());
		if (billingAddress != null && !billingAddress.isEmpty())
			build.add("BillingAddress", billingAddress.toJsonBuilder());
		return build;
	}

	public boolean isEmpty() {
		return ipAddress == null && languageCode == null && deliveryAddress == null && billingAddress == null;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

}
