package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class CardForm {

	/** string	
	 * This parameter let you customize the holder name field on the card entry form. Per default, a mandatory holder name field is shown.
	 * Possible values: NONE, MANDATORY.
	 * Example: MANDATORY */
	private HolderName holderName;
	
	public static CardForm get(JsonObject object) {
		if (object != null) {
			String var;
			CardForm obj = new CardForm();
			var = object.getString("HolderName", null);
			if (var != null) obj.holderName = HolderName.valueOf(var);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (holderName != null) build.add("HolderName", holderName.name());
		return build;
	}

	public boolean isEmpty() {
		return getHolderName() == null;
	}

	public HolderName getHolderName() {
		return holderName;
	}

	public void setHolderName(HolderName holderName) {
		this.holderName = holderName;
	}
	
}
