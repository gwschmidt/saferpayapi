package ch.performancebuildings.saferpay.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class Transaction {

	/**
	 * mandatory, string Type of transaction. One of 'PAYMENT', 'REFUND' Possible
	 * values: PAYMENT, REFUND. Example: PAYMENT
	 */
	private TransactionType type;
	/**
	 * mandatory, string Current status of the transaction. One of 'AUTHORIZED',
	 * 'CAPTURED' Possible values: AUTHORIZED, CAPTURED. Example: AUTHORIZED
	 */
	private TransactionStatus status;
	/**
	 * mandatory, string Unique Saferpay transaction id. Used to reference the
	 * transaction in any further step. Example: K5OYS9Ad6Ex4rASU1IM1b3CEU8bb
	 */
	private String id;
	/**
	 * mandatory, date Date / time of the authorization Example:
	 * 2011-09-23T14:57:23.023+02.00
	 */
	private LocalDate date;
	/**
	 * mandatory, container Amount (currency, value, etc.) that has been authorized.
	 */
	private Amount amount;
	/**
	 * string OrderId given with request Id[1..80] Example:
	 * c52ad18472354511ab2c33b59e796901
	 */
	private String orderId;
	/**
	 * string Name of the acquirer Example: AcquirerName
	 */
	private String acquirerName;
	/**
	 * string Reference id of the acquirer (if available) Example: AcquirerReference
	 */
	private String acquirerReference;
	/**
	 * container Direct Debit information, if applicable Example: AcquirerReference
	 */
	private DirectDebit directDebit;
	/**
	 * container Invoice information, if applicable
	 */
	private Invoice invoice;

	public Transaction() {
		super();
	}

	public Transaction(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		type = TransactionType.valueOf(object.getString("Type"));
		status = TransactionStatus.valueOf(object.getString("Status"));
		id = object.getString("Id");
		date = LocalDate.parse(object.getString("Date"), DateTimeFormatter.ISO_DATE_TIME);
		amount = new Amount("Transaction.Amount", object.getJsonObject("Amount"));
		orderId = object.getString("orderId", null);
		acquirerName = object.getString("AcquirerName", null);
		acquirerReference = object.getString("AcquirerReference", null);
		directDebit = DirectDebit.get(object.getJsonObject("DirectDebit"));
		invoice = Invoice.get(object.getJsonObject("Invoice"));
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Type", type.name());
		build.add("Status", status.name());
		build.add("Id", id);
		build.add("Date", date.format(DateTimeFormatter.ISO_DATE_TIME));
		build.add("Amount", amount.toJsonBuilder());
		if (orderId != null)
			build.add("OrderId", orderId);
		if (acquirerName != null)
			build.add("AcquirerName", acquirerName);
		if (acquirerReference != null)
			build.add("AcquirerReference", orderId);
		if (directDebit != null)
			build.add("DirectDebit", directDebit.toJsonBuilder());
		if (invoice != null && !invoice.isEmpty())
			build.add("Invoice", invoice.toJsonBuilder());
		return build;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public TransactionStatus getStatus() {
		return status;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Amount getAmount() {
		return amount;
	}

	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAcquirerName() {
		return acquirerName;
	}

	public void setAcquirerName(String acquirerName) {
		this.acquirerName = acquirerName;
	}

	public String getAcquirerReference() {
		return acquirerReference;
	}

	public void setAcquirerReference(String acquirerReference) {
		this.acquirerReference = acquirerReference;
	}

	public DirectDebit getDirectDebit() {
		return directDebit;
	}

	public void setDirectDebit(DirectDebit directDebit) {
		this.directDebit = directDebit;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}
