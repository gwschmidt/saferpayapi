package ch.performancebuildings.saferpay.model;

import java.util.EnumSet;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;
import ch.performancebuildings.saferpay.util.Util;

public class InitializeRequest {

	/**
	 * mandatory, container General information about the request.
	 */
	private RequestHeader requestHeader;
	/**
	 * mandatory, string Saferpay terminal id Numeric[8..8] Example: 12345678
	 */
	private String terminalId;
	/**
	 * mandatory, container Information about the payment (amount, currency, ...)
	 */
	private Payment payment;
	/**
	 * string[] Used to restrict the means of payment which are available to the
	 * payer for this transaction. If only one payment method id is set, the payment
	 * selection step will be skipped. Possible values: AMEX, DIRECTDEBIT, INVOICE,
	 * BONUS, DINERS, EPRZELEWY, EPS, GIROPAY, IDEAL, JCB, MAESTRO, MASTERCARD,
	 * MYONE, PAYPAL, POSTCARD, POSTFINANCE, SAFERPAYTEST, SOFORT, VISA, VPAY.
	 * Example: ["VISA", "MASTERCARD"]
	 */
	private EnumSet<PaymentMethod> paymentMethods;
	/**
	 * string[] Used to control if wallets should be enabled on the payment
	 * selection page and to go directly to the given wallet (if exactly one wallet
	 * is filled and PaymentMethods is not set). Possible values: MASTERPASS.
	 * Example: ["MASTERPASS"]
	 */
	private EnumSet<Wallet> wallets;
	/**
	 * container Information about the payer
	 */
	private Payer payer;
	/**
	 * container If the given means of payment should be stored in Saferpay Secure
	 * Card Data storage (if applicable)
	 */
	private RegisterAlias registerAlias;
	/**
	 * mandatory, container Urls which are to be used to redirect the payer back to
	 * the shop if the transaction requires some kind of browser redirection
	 * (3d-secure, dcc)
	 */
	private ReturnUrls returnUrls;
	/**
	 * container Notification options
	 */
	private Notification notification;
	/**
	 * container Styling options
	 */
	private Styling styling;
	/**
	 * container Used to have the payer enter his address in the payment process.
	 * Only one address form is supported at this time!
	 */
	private AddressForm billingAddressForm;
	/**
	 * container Used to have the payer enter his address in the payment process.
	 * Only one address form is supported at this time!
	 */
	private AddressForm deliveryAddressForm;
	/**
	 * container Options for card data entry form (if applicable)
	 */
	private CardForm cardForm;

	public InitializeRequest() {
		super();
	}
	
	public InitializeRequest(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		requestHeader = new RequestHeader("InitializeRequest.RequestHeader", object.getJsonObject("RequestHeader"));
		terminalId = object.getString("TerminalId");
		payment = new Payment("InitializeRequest.Payment", object.getJsonObject("Payment"));
		paymentMethods = Util.getEnumSet(object.getJsonArray("PaymentMethods"), PaymentMethod.class);
		wallets = Util.getEnumSet(object.getJsonArray("Wallets"), Wallet.class);
		payer = Payer.get(object.getJsonObject("Payer"));
		registerAlias = RegisterAlias.get(object.getJsonObject("RegisterAlias"));
		returnUrls = new ReturnUrls("InitializeRequest.ReturnUrls", object.getJsonObject("ReturnUrls"));
		notification = Notification.get(object.getJsonObject("Notification"));
		styling = Styling.get(object.getJsonObject("Styling"));
		billingAddressForm = AddressForm.get(object.getJsonObject("BillingAddressForm"));
		deliveryAddressForm = AddressForm.get(object.getJsonObject("DeliveryAddressForm"));
		cardForm = CardForm.get(object.getJsonObject("CardForm"));
	}
	
	
	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("RequestHeader", requestHeader.toJsonBuilder());
		build.add("TerminalId", terminalId);
		build.add("Payment", payment.toJsonBuilder());
		if (paymentMethods != null) build.add("PaymentMethods", Util.getJsonArray(paymentMethods));
		if (wallets != null) build.add("Wallets", Util.getJsonArray(wallets));
		if (payer != null) build.add("Payer", payer.toJsonBuilder());
		if (registerAlias != null) build.add("registerAlias", registerAlias.toJsonBuilder());
		build.add("ReturnUrls", returnUrls.toJsonBuilder());
		if (notification != null && !notification.isEmpty()) build.add("Notification", notification.toJsonBuilder());
		if (styling != null && !styling.isEmpty()) build.add("Styling", styling.toJsonBuilder());
		if (billingAddressForm != null) build.add("BillingAddressForm", billingAddressForm.toJsonBuilder());
		if (deliveryAddressForm != null) build.add("DeliveryAddressForm", deliveryAddressForm.toJsonBuilder());
		if (cardForm != null && !cardForm.isEmpty() ) build.add("CardForm", cardForm.toJsonBuilder());
		return build;
	}

	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public EnumSet<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(EnumSet<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public EnumSet<Wallet> getWallets() {
		return wallets;
	}

	public void setWallets(EnumSet<Wallet> wallets) {
		this.wallets = wallets;
	}

	public Payer getPayer() {
		return payer;
	}

	public void setPayer(Payer payer) {
		this.payer = payer;
	}

	public RegisterAlias getRegisterAlias() {
		return registerAlias;
	}

	public void setRegisterAlias(RegisterAlias registerAlias) {
		this.registerAlias = registerAlias;
	}

	public ReturnUrls getReturnUrls() {
		return returnUrls;
	}

	public void setReturnUrls(ReturnUrls returnUrls) {
		this.returnUrls = returnUrls;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public Styling getStyling() {
		return styling;
	}

	public void setStyling(Styling styling) {
		this.styling = styling;
	}

	public AddressForm getDeliveryAddressForm() {
		return deliveryAddressForm;
	}

	public void setDeliveryAddressForm(AddressForm deliveryAddressForm) {
		this.deliveryAddressForm = deliveryAddressForm;
	}

	public CardForm getCardForm() {
		return cardForm;
	}

	public void setCardForm(CardForm cardForm) {
		this.cardForm = cardForm;
	}

}
