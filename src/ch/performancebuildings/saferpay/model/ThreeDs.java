package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class ThreeDs {

	/**
	 * mandatory, boolean Indicates, whether the payer has successfuly authenticated
	 * him/herself or not.
	 */
	private boolean authenticated;
	/**
	 * mandatory, boolean Indicates whether liability shift to issuer is possible or
	 * not. Not present if PaymentMeans container was not present in Initialize
	 * request. True, if liability shift to issuer is possible, false if not (only
	 * SSL transaction). Please note, that the authentification can be true, but the
	 * liabilityshift is false. The issuer has the right to deny the liabiliy shift
	 * during the authorization. You can continue to capture the payment here, but
	 * we recommend to cancel unsecure payments.
	 */
	private boolean liabilityShift;
	/**
	 * mandatory, string BASE64 encoded value, given by the MPI. References the 3D
	 * Secure process. Example: ARkvCgk5Y1t/BDFFXkUPGX9DUgs=
	 */
	private String xid;
	/**
	 * string Mandatory if Authenticated is true Example:
	 * AAABBIIFmAAAAAAAAAAAAAAAAAA=
	 */
	private String verificationValue;

	public static ThreeDs get(JsonObject object) {
		if (object != null) {
			ThreeDs obj = new ThreeDs();
			obj.authenticated = object.getBoolean("Authenticated");
			obj.liabilityShift = object.getBoolean("LiabilityShift");
			obj.xid = object.getString("Xid");
			obj.verificationValue = object.getString("VerificationValue", null);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Authenticated", authenticated);
		build.add("LiabilityShift", liabilityShift);
		build.add("Xid", xid);
		if (verificationValue != null)
			build.add("VerificationValue", verificationValue);
		return build;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		this.authenticated = authenticated;
	}

	public boolean isLiabilityShift() {
		return liabilityShift;
	}

	public void setLiabilityShift(boolean liabilityShift) {
		this.liabilityShift = liabilityShift;
	}

	public String getXid() {
		return xid;
	}

	public void setXid(String xid) {
		this.xid = xid;
	}

	public String getVerificationValue() {
		return verificationValue;
	}

	public void setVerificationValue(String verificationValue) {
		this.verificationValue = verificationValue;
	}

}
