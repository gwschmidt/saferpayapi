package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class Payment {

	/**
	 * mandatory, container Amount data (currency, value, etc.)
	 */
	private Amount amount;
	/**
	 * string Unambiguous order identifier defined by the merchant/ shop. This
	 * identifier might be used as reference later on. Id[1..80] Example:
	 * c52ad18472354511ab2c33b59e796901
	 */
	private String orderId;
	/**
	 * mandatory, string A human readable description provided by the merchant that
	 * will be displayed in Payment Page. Utf8[1..1000] Example: Description of
	 * payment
	 */
	private String description;
	/**
	 * string Text which will be printed on payere's debit note. Supported by SIX
	 * Acquiring. No guarantee, that it will show up on the payer's debit note,
	 * because his bank has to support it too. Also note, that 30 characters rarely
	 * are supported. It's usually around 10-12. Utf8[1..50] Example: Payernote
	 */
	private String payerNote;
	/**
	 * string Mandate reference of the payment. Needed for German direct debits
	 * (ELV) only. The value has to be unique. Example: MandateId
	 */
	private String mandateId;
	/**
	 * container Specific payment options
	 */
	private Options options;
	/**
	 * container Recurring options � cannot be combined with Installment.
	 */
	private Recurring recurring;
	/**
	 * container Installment options � cannot be combined with Recurring.
	 */
	private Installment installment;

	public Payment() {
		super();
	}

	public Payment(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		amount = new Amount("Payment.Amount", object.getJsonObject("Amount"));
		orderId = object.getString("OrderId", null);
		description = object.getString("Description");
		payerNote = object.getString("PayerNote", null);
		mandateId = object.getString("MandateId", null);
		options = Options.get(object.getJsonObject("Options"));
		recurring = Recurring.get(object.getJsonObject("Recurring"));
		installment = Installment.get(object.getJsonObject("Installment"));
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Amount", amount.toJsonBuilder());
		if (orderId != null)
			build.add("OrderId", orderId);
		build.add("Description", description);
		if (payerNote != null)
			build.add("PayerNote", payerNote);
		if (mandateId != null)
			build.add("MandateId", mandateId);
		if (options != null && !options.isEmpty())
			build.add("Options", options.toJsonBuilder());
		if (recurring != null && !options.isEmpty())
			build.add("Recurring", recurring.toJsonBuilder());
		if (installment != null && !options.isEmpty())
			build.add("Installment", installment.toJsonBuilder());
		return build;
	}

	public Amount getAmount() {
		return amount;
	}

	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPayerNote() {
		return payerNote;
	}

	public void setPayerNote(String payerNote) {
		this.payerNote = payerNote;
	}

	public String getMandateId() {
		return mandateId;
	}

	public void setMandateId(String mandateId) {
		this.mandateId = mandateId;
	}

	public Options getOptions() {
		return options;
	}

	public void setOptions(Options options) {
		this.options = options;
	}

	public Recurring getRecurring() {
		return recurring;
	}

	public void setRecurring(Recurring recurring) {
		this.recurring = recurring;
	}

	public Installment getInstallment() {
		return installment;
	}

	public void setInstallment(Installment installment) {
		this.installment = installment;
	}

}
