package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class ResponseHeader {

	/**
	 * RequestId of the original request header Id[1..50] Example:
	 * 33e8af17-35c1-4165-a343-c1c86a320f3b mandatory
	 */
	private String requestId;
	/**
	 * Version number of the interface specification. Possible values: 1.0, 1.1,
	 * 1.2. Example: 1.0 mandatory
	 */
	private String specVersion;

	public ResponseHeader() {
		super();
	}

	public ResponseHeader(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		requestId = object.getString("RequestId");
		specVersion = object.getString("SpecVersion");
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("RequestId", requestId);
		build.add("SpecVersion", specVersion);
		return build;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getSpecVersion() {
		return specVersion;
	}

	public void setSpecVersion(String specVersion) {
		this.specVersion = specVersion;
	}

}
