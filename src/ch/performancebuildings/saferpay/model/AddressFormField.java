package ch.performancebuildings.saferpay.model;

public enum AddressFormField {

	CITY, 
	COMPANY, 
	COUNTRY, 
	EMAIL, 
	FIRSTNAME, 
	LASTNAME, 
	PHONE, 
	SALUTATION, 
	STATE, 
	STREET, 
	ZIP
}
