package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class Dcc {

	/**
	 * mandatory, container Amount in payer�s currency
	 */
	private Amount payerAmount;

	public static Dcc get(JsonObject object) throws SaferPayError {
		if (object != null) {
			Dcc obj = new Dcc();
			obj.payerAmount = new Amount("Dcc.PayerAmount", object.getJsonObject("PayerAmount"));
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("PayerAmount", payerAmount.toJsonBuilder());
		return build;
	}

	public Amount getPayerAmount() {
		return payerAmount;
	}

	public void setPayerAmount(Amount payerAmount) {
		this.payerAmount = payerAmount;
	}

}
