package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class AssertResponse {

	/**
	 * mandatory, container Contains general informations about the response.
	 */
	private ResponseHeader responseHeader;
	/**
	 * mandatory, container Information about the transaction
	 */
	private Transaction transaction;
	/**
	 * mandatory, container Information about the means of payment
	 */
	private PaymentMeans paymentMeans;
	/**
	 * container Information about the payer / card holder
	 */
	private Payer payer;
	/**
	 * container Information about the SCD registration outcome
	 */
	private RegistrationResult registrationResult;
	/**
	 * container 3d-secure information if applicable
	 */
	private ThreeDs threeDs;
	/**
	 * container Dcc information, if applicable
	 */
	private Dcc dcc;

	public AssertResponse() {
		super();
	}
	
	public AssertResponse(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		responseHeader = new ResponseHeader("AssertResponse.ResponseHeader", object.getJsonObject("ResponseHeader"));
		payer = Payer.get(object.getJsonObject("Payer"));
		dcc = Dcc.get(object.getJsonObject("Dcc"));
		threeDs = ThreeDs.get(object.getJsonObject("ThreeDs"));
		transaction = new Transaction("AssertResponse.Transaction", object.getJsonObject("Transaction"));
		registrationResult = RegistrationResult.get(object.getJsonObject("RegistrationResult"));
		paymentMeans = new PaymentMeans("AssertResponse.PaymentMeans", object.getJsonObject("PaymentMeans"));
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("ResponseHeader", responseHeader.toJsonBuilder());
		if (payer != null)
			build.add("Payer", payer.toJsonBuilder());
		if (dcc != null)
			build.add("Dcc", dcc.toJsonBuilder());
		if (threeDs != null)
			build.add("ThreeDs", threeDs.toJsonBuilder());
		build.add("Transaction", transaction.toJsonBuilder());
		if (registrationResult != null)
			build.add("RegistrationResult", registrationResult.toJsonBuilder());
		if (paymentMeans != null)
			build.add("PaymentMeans", paymentMeans.toJsonBuilder());
		return build;
	}

	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

	public Payer getPayer() {
		return payer;
	}

	public void setPayer(Payer payer) {
		this.payer = payer;
	}

	public Dcc getDcc() {
		return dcc;
	}

	public void setDcc(Dcc dcc) {
		this.dcc = dcc;
	}

	public ThreeDs getThreeDs() {
		return threeDs;
	}

	public void setThreeDs(ThreeDs threeDs) {
		this.threeDs = threeDs;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public RegistrationResult getRegistrationResult() {
		return registrationResult;
	}

	public void setRegistrationResult(RegistrationResult registrationResult) {
		this.registrationResult = registrationResult;
	}

	public PaymentMeans getPaymentMeans() {
		return paymentMeans;
	}

	public void setPaymentMeans(PaymentMeans paymentMeans) {
		this.paymentMeans = paymentMeans;
	}

}
