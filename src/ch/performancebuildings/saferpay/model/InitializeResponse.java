package ch.performancebuildings.saferpay.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class InitializeResponse {

	/**
	 * mandatory, container Contains general informations about the response.
	 */
	private ResponseHeader responseHeader;
	/**
	 * mandatory, string Token for later referencing Example:
	 * 234uhfh78234hlasdfh8234e1234
	 */
	private String token;
	/**
	 * mandatory, date Expiration date / time of the generated token in ISO 8601
	 * format in UTC. After this time, the token won�t be accepted for any further
	 * action. Example: 2011-07-14T19:43:37+01:00
	 */
	private LocalDateTime expiration;
	/**
	 * mandatory, string Redirecturl for the payment page transaction. Simply add
	 * this to a "Pay Now"-button or do an automatic redirect. Example:
	 * https://www.saferpay.com/vt2/api/PaymentPage/1234/12341234/z2p7a0plpgsd41m97wjvm5jza
	 */
	private String redirectUrl;

	public InitializeResponse() {
		super();
	}
	
	public InitializeResponse(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		responseHeader = new ResponseHeader("InitializeResponse.ResponseHeader", object.getJsonObject("ResponseHeader"));
		token = object.getString("token");
		expiration = LocalDateTime.parse(object.getString("Expiration"), DateTimeFormatter.ISO_ZONED_DATE_TIME);
		redirectUrl = object.getString("RedirectUrl");
	}
	
	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("RequestHeader", responseHeader.toJsonBuilder());
		build.add("Token", token);
		build.add("Expiration", expiration.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
		build.add("RedirectUrl", redirectUrl);
		return build;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String url) {
		this.redirectUrl = url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getExpiration() {
		return expiration;
	}

	public void setExpiration(LocalDateTime expiration) {
		this.expiration = expiration;
	}

	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

}
