package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Options {

	/**
	 * boolean Used to flag the transaction as a Pre-Athorization. This type of
	 * transaction is only supported with the following Acquiring: SIX Payment
	 * Services, B+S Card Service, ConCardis, Airplus and -after an agreement- with
	 * American Express.
	 */
	private Boolean preAuth;

	public static Options get(JsonObject object) {
		if (object != null) {
			Options obj = new Options();
			obj.preAuth = object.getBoolean("PreAuth", false);
			return obj;
		}
		else {
			return null;
		}
	}

	public Options() {
		super();
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (preAuth != null)
			build.add("PreAuth", preAuth);
		return build;
	}

	public boolean isEmpty() {
		return preAuth == null;
	}

	public Boolean getPreAuth() {
		return preAuth;
	}

	public void setPreAuth(Boolean preAuth) {
		this.preAuth = preAuth;
	}

}
