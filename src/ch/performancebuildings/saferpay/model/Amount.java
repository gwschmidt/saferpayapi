package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class Amount {

	/**
	 * mandatory, string Amount in minor unit (CHF 1.00 ? Value=100) Example: 100
	 */
	private String value;
	/**
	 * mandatory, string ISO 4217 3-letter currency code (CHF, USD, EUR, ...)
	 * Example: CHF
	 */
	private String currencyCode;

	public Amount() {
		super();
	}

	public Amount(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		value = object.getString("Value");
		currencyCode = object.getString("CurrencyCode");
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("Value", value);
		build.add("CurrencyCode", currencyCode);
		return build;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
