package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class AssertRequest {

	/**
	 * mandatory, container General information about the request.
	 */
	private RequestHeader requestHeader;
	/**
	 * mandatory, string Token returned by initial call. Id[1..50] Example:
	 * 234uhfh78234hlasdfh8234e
	 */
	private String token;

	public AssertRequest() {
		super();
	}
	
	public AssertRequest(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		requestHeader = new RequestHeader("AssertRequest.RequestHeader", object.getJsonObject("RequestHeader"));
		token = object.getString("Token");
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		build.add("RequestHeader", requestHeader.toJsonBuilder());
		build.add("Token", token);
		return build;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;
	}

}
