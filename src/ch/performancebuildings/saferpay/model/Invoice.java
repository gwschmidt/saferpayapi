package ch.performancebuildings.saferpay.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Invoice {

	/**
	 * container Information about the payee, eg billpay, who is responsible for
	 * collecting the bill
	 */
	private Payee payee;
	/**
	 * string The reason for transfer to be stated when paying the invoice (transfer
	 * of funds)
	 */
	private String reasonForTransfer;
	/**
	 * date The date by which the invoice needs to be settled
	 */
	private LocalDate dueDate;

	public static Invoice get(JsonObject object) {
		if (object != null) {
			String var;
			Invoice obj = new Invoice();
			obj.payee = Payee.get(object.getJsonObject("Payee"));
			obj.reasonForTransfer = object.getString("ReasonForTransfer", null);
			var = object.getString("dueDate", null);
			if (var != null)
				obj.dueDate = LocalDate.parse(var, DateTimeFormatter.ISO_DATE);
			return obj;
		}
		else {
			return null;
		}
	}

	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (payee != null)
			build.add("Payee", payee.toJsonBuilder());
		if (reasonForTransfer != null)
			build.add("ReasonForTransfer", reasonForTransfer);
		if (dueDate != null)
			build.add("DueDate", dueDate.format(DateTimeFormatter.ISO_DATE));
		return build;
	}

	public boolean isEmpty() {
		return payee == null && reasonForTransfer == null && dueDate == null;
	}

	public String getReasonForTransfer() {
		return reasonForTransfer;
	}

	public void setReasonForTransfer(String reasonForTransfer) {
		this.reasonForTransfer = reasonForTransfer;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

}
