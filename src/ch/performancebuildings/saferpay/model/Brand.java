package ch.performancebuildings.saferpay.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.performancebuildings.saferpay.util.SaferPayError;

public class Brand {

	/** string	
	 * alphanumeric id of the payment method / brand
	 * Possible values: AMEX, DIRECTDEBIT, INVOICE, BONUS, DINERS, EPRZELEWY, EPS, GIROPAY, IDEAL, JCB, MAESTRO, 
	 * MASTERCARD, MYONE, PAYPAL, POSTCARD, POSTFINANCE, SAFERPAYTEST, SOFORT, VISA, VPAY.*/
	private PaymentMethod paymentMethod;
	/** mandatory, string	
	 * Name of the Brand (Visa, Mastercard, an so on � might change over time, only use for display, never for parsing). Only use it for display, never for parsing and/or mapping! Use PaymentMethod instead.
	 * Example: SaferpayTestCard */
	private String name;
	
	public Brand(String fieldName, JsonObject object) throws SaferPayError {
		if (object == null) throw new SaferPayError(fieldName, true);
		String var;
		var = object.getString("PaymentMethod", null);
		if (var != null) paymentMethod = PaymentMethod.valueOf(var);
		name = object.getString("Name");
	}
	
	public JsonObjectBuilder toJsonBuilder() {
		JsonObjectBuilder build = Json.createObjectBuilder();
		if (paymentMethod != null) build.add("PaymentMethod", paymentMethod.name());
		build.add("Name", name);
		return build;
	}
	
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
